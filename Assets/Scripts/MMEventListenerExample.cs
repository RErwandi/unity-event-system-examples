﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class MMEventListenerExample : MonoBehaviour, MMEventListener<MMKeyboardEvent>
{
    private void OnEnable()
    {
        this.MMEventStartListening();
    }

    private void OnDisable()
    {
        this.MMEventStopListening();
    }

    void ChangeColor()
    {
        GetComponent<Renderer>().material.color = Color.red;
    }

    public void OnMMEvent(MMKeyboardEvent eventType)
    {
        throw new NotImplementedException();
    }
}
